# NBC Discretization

This code was used for experimenting with discretization in combination with the Naive Bayes Classifier. It was part of a research project. 

The code currently only runs on Linux, but by removing the doMC package and removing the parallelism, can be made to run on other operating systems as wel.


Daan Knoope, 2018