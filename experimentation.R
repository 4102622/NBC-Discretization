#!/usr/bin/env Rscript

if (!require("pacman")) {install.packages("pacman")}
library("pacman")
pacman::p_load("caret", "klaR", "e1071", "classInt", "foreach", "discretization", "doMC", install=TRUE)
trainingPercentage = 0.6

# parallelism, this only works on linux
registerDoMC(52)

source("models.R")
source("dsTools.R")

methods = c("EFD","EWD","Continuous", "MDLP")#, "chiM")

reps = 50

getScenarios <- function(){
  scenarios <- list()
    scenarios$TwoSmallScene    <- createArtificialDataset(100, 2, 1.0, 1.0, 0.05)
    scenarios$TwoModerateScene <-  createArtificialDataset(1000, 2, 1.0, 1.0, 0.05)
    scenarios$TwoLargeScene    <- createArtificialDataset(10000,2,1.0,1.0,0.05)
    
    scenarios$ThreeSmallScene     <- createArtificialDataset(100, 3, 1.0, 1.0, 0.05)
    scenarios$ThreeModerateScene  <-  createArtificialDataset(1000, 3, 1.0, 1.0, 0.05)
    scenarios$ThreeLargeScene     <- createArtificialDataset(10000,3,1.0,1.0,0.05)
    
    scenarios$TwoSmallNoiselessScene    <- createArtificialDataset(100, 2, 1.0, 1.0, 0.0)
    scenarios$TwoModerateNoiselessScene <-  createArtificialDataset(1000, 2, 1.0, 1.0, 0.0)
    scenarios$TwoLargeNoiselessScene    <- createArtificialDataset(10000,2,1.0,1.0,0.0)
    
    scenarios$ThreeSmallNoiselessScene    <- createArtificialDataset(100, 3, 1.0, 1.0, 0.0)
    scenarios$ThreeModerateNoiselessScene <-  createArtificialDataset(1000, 3, 1.0, 1.0, 0.0)
    scenarios$ThreeLargeNoiselessScene    <- createArtificialDataset(10000,3,1.0,1.0,0.0)
    
    scenarios$TwoModerateNoiseScene   <- createArtificialDataset(1000, 2, 1.0, 1.0, 0.25)
    scenarios$ThreeModerateNoiseScene <- createArtificialDataset(1000, 3, 1.0, 1.0, 0.25)
    
    
    scenarios$TwoCloseScene <- createArtificialDataset(1000, 2, 0.1, 1.0, 0.05)
    scenarios$ThreeCloseScene <- createArtificialDataset(1000, 3, 0.1, 1.0, 0.05)
    
    scenarios$TwoNoisyCloseScene <- createArtificialDataset(1000, 2, 0.1, 1.0, 0.25)
    scenarios$ThreeNoisyCloseScene <- createArtificialDataset(1000, 3, 0.1, 1.0, 0.25)
    
    scenarios$TwoNoiselessCloseScene <- createArtificialDataset(1000,2, 0.1, 1.0, 0)
    scenarios$ThreeNoiselessCloseScene <- createArtificialDataset(1000,3, 0.1, 1.0, 0)
  
  return(scenarios)
}

getMethodTable <- function(ds, n, trainingIndices, method){
  if(method=="EFD"){
    return(getFrequencyTable(ds,n,trainingIndices))
  }
  if(method=="Continuous"){
    return(getContinuousTable(ds,trainingIndices))
  }
  if(method=="EWD"){
    return(getWidthTable(ds,n,trainingIndices))
  }
  if(method=="MDLP"){
    return(getMdlpTable(ds,trainingIndices))
  }
  if(method=="chiM"){
    return(getChimTable(ds, trainingIndices))
  }
  stop("Method \"" + method + "\" is not (yet) implemented")
}

scenarios <- getScenarios()

print("SizePerClass,Distance,Error,Method,Accuracy,Precision,Recall,F1,Time")

resultC <- list()
resultC <- foreach(i=1:reps, .combine=c)%dopar%{
  scenarios = getScenarios()
  result <- foreach(scenario=names(scenarios), .combine=c) %do% {
    ds <- scenarios[[scenario]]
    nBin = 10 # move this to scenarios
    trainingIndices = sample(1:nrow(ds), trainingPercentage*nrow(ds), replace=FALSE)
    
    foreach(method=methods, .combine=c) %do%{
      t <- system.time(table <- getMethodTable(ds, nBin, trainingIndices, method))[[3]]
      if(is.na(table)){
        acc  <- NA
        prec <- NA
        rec  <- NA
        f1   <- NA
      } else{
        acc = table$overall["Accuracy"]
        
        if(class(table$byClass[5]) == "numeric"){
          prec <- table$byClass[5]
          rec  <- table$byClass[6]
          f1   <- table$byClass[7]
        } else {
          precs = table$byClass[5,]
          precs[is.na(precs)] <- 0
          prec <- mean(precs)
          
          recs = table$byClass[6,]
          recs[is.na(recs)] <- 0
          rec <- mean(recs)
          
          f1s = table$byClass[7,]
          f1s[is.na(f1s)] <- 0
          f1 <- mean(f1s)
        }
      }
      paste(scenario,method,acc,prec,rec,f1,t,sep=",")
    }
  }
}

write(resultC,"results.csv",sep="\n")
